import { useContext, useEffect,useState } from "react"
import { ExchangeContext } from "../../contexts/ExchangeContext";
import { GameFieldBlock } from "./element/GameFieldBlock";
import MogalAlerGame from "./element/MogalAlerGame";
import { GamePannel } from "./GamePannel";
function Game() {

    const {win,setWin,bet,balance,setBalance,setBlock,block,fieldSize,fieldArr,ifActiveGame,setActiveGame,countMine} = useContext(ExchangeContext);

    const [showModalAlerGame, setShowModalAlerGame] = useState(false);

    const [resalt, setResalt] = useState(block);

    useEffect(()=>{
        if(resalt===1){
            setActiveGame()
            setResalt(null)
            setShowModalAlerGame(true)
            setBalance(balance-bet)
        }  
    },[resalt,block])
    const play = (id) => {
        if(id===0){
            setWin(win+win*(countMine/fieldSize/fieldSize))
        } 
    }
    return <>
        <div className="game-container">
                    {!ifActiveGame ? <div className="presbet">PRESS BET TO PLAY</div> : null}
                <ul className="game-field">
                    {fieldArr.map(el=>(
                        <GameFieldBlock play={play} ifActiveGame={ifActiveGame}setBlock={setBlock}key={el.count} block={block} setResalt={setResalt} {...el} fieldSize={fieldSize}/>
                    ))}
            </ul>
            <MogalAlerGame show={showModalAlerGame} onHide={() => setShowModalAlerGame(false)} />
            <GamePannel />      
        </div>
    </>
}
export {Game}