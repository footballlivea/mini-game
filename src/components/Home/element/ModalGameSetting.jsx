import React from "react";
// import { useTranslation } from "react-i18next";
import { Button, Modal } from "react-bootstrap";
import { useContext, useEffect,useState } from "react"
import { ExchangeContext } from "../../../contexts/ExchangeContext";

const ModalGameSetting = (props) => {
  // Translation
  // const { t } = useTranslation();
  const {setBlock,setFieldSize,setFieldArr,fieldSize,countMine,setCountMine} = useContext(ExchangeContext);

  const [size, setSize] = useState(fieldSize);
  const [mine, setMine] = useState(countMine);
  
    useEffect(()=>{
        setFieldSize(size)
        setFieldArr()
        setBlock(null)
    },[size,mine])
    useEffect(()=>{
      setCountMine(mine)
      setFieldArr()
      setBlock(null)
    },[mine])

  return (
    <Modal className="game-setting-pannel" {...props} size="md" aria-labelledby="contained-modal-title-vcenter" centered>
      <Modal.Header closeButton>Настройки игрового поля</Modal.Header>
      <Modal.Body>
      <p>РАЗМЕР СЕТКИ</p>
       <Button className={size===3 ? 'btn-setting activeBlock' : 'btn-setting'} value="3" onClick={(e) => setSize(Number(e.target.value))}>3x3</Button>
       <Button className={size===5 ? 'btn-setting activeBlock' : 'btn-setting'} value="5" onClick={(e) => setSize(Number(e.target.value))}>5x5</Button>
       <Button className={size===7 ? 'btn-setting activeBlock' : 'btn-setting'} value="7" onClick={(e) => setSize(Number(e.target.value))}>7x7</Button>
       <Button className={size===9 ? 'btn-setting activeBlock' : 'btn-setting'} value="9" onClick={(e) => setSize(Number(e.target.value))}>9x9</Button>
      </Modal.Body>
      <Modal.Body>
      <p>КОЛИЧЕСТВО МИН</p>
      { 
       size===3 ? 
       <><Button value={1} onClick={(e) => setMine(Number(e.target.value))} className={countMine===1 ? 'btn-setting activeBlock' : 'btn-setting'}>1</Button>
       <Button value={2} onClick={(e) => setMine(Number(e.target.value))} className={countMine===2 ? 'btn-setting activeBlock' : 'btn-setting'}>2</Button>
       <Button value={3} onClick={(e) => setMine(Number(e.target.value))}  className={countMine===3 ? 'btn-setting activeBlock' : 'btn-setting'}>3</Button>
       <Button value={4} onClick={(e) => setMine(Number(e.target.value))}  className={countMine===4 ? 'btn-setting activeBlock' : 'btn-setting'}>4</Button></>
       : size===5 ?
       <><Button value={1} onClick={(e) => setMine(Number(e.target.value))}  className={countMine===1 ? 'btn-setting activeBlock' : 'btn-setting'}>1</Button>
       <Button value={3} onClick={(e) => setMine(Number(e.target.value))}  className={countMine===3 ? 'btn-setting activeBlock' : 'btn-setting'}>3</Button>
       <Button value={5} onClick={(e) => setMine(Number(e.target.value))}  className={countMine===5 ? 'btn-setting activeBlock' : 'btn-setting'}>5</Button>
       <Button value={7} onClick={(e) => setMine(Number(e.target.value))}  className={countMine===7 ? 'btn-setting activeBlock' : 'btn-setting'}>7</Button></>
       : size===7 ?
       <><Button value={1} onClick={(e) => setMine(Number(e.target.value))}  className={countMine===1 ? 'btn-setting activeBlock' : 'btn-setting'}>1</Button>
       <Button value={5} onClick={(e) => setMine(Number(e.target.value))}  className={countMine===5 ? 'btn-setting activeBlock' : 'btn-setting'}>5</Button>
       <Button value={10} onClick={(e) => setMine(Number(e.target.value))}  className={countMine===10 ? 'btn-setting activeBlock' : 'btn-setting'}>10</Button>
       <Button value={15} onClick={(e) => setMine(Number(e.target.value))}  className={countMine===15 ? 'btn-setting activeBlock' : 'btn-setting'}>15</Button></>
       : size===9 ?
       <><Button value={5} onClick={(e) => setMine(Number(e.target.value))}  className={countMine===5 ? 'btn-setting activeBlock' : 'btn-setting'}>5</Button>
       <Button value={10} onClick={(e) => setMine(Number(e.target.value))}  className={countMine===10 ? 'btn-setting activeBlock' : 'btn-setting'}>10</Button>
       <Button value={15} onClick={(e) => setMine(Number(e.target.value))}  className={countMine===15 ? 'btn-setting activeBlock' : 'btn-setting'}>15</Button>
       <Button value={20} onClick={(e) => setMine(Number(e.target.value))}  className={countMine===20 ? 'btn-setting activeBlock' : 'btn-setting'}>20</Button></>
       :null
      }
      </Modal.Body>
    </Modal>
  );
};

export default ModalGameSetting;
