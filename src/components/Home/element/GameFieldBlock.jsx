import { useContext, useEffect,useState } from "react"

function GameFieldBlock(props) {
    const {play,setBlock,block,id,fieldSize,setResalt,ifActiveGame} = props

    const [typeOfBlock, setTypeOfBlock] = useState(null);

    const open = () => {
        if(!ifActiveGame)return
        setTypeOfBlock(id)
        setResalt(id)
        setBlock(id)
        play(id)
    };

    useEffect(()=>{
        if(block===null) setTypeOfBlock(null)
    },[block])

    return <>
        <li className={typeOfBlock===0 ? 'activeBlock' : typeOfBlock===1 ? 'activeBlockFail' : null} onClick={open} style={{width : `calc(350px / ${fieldSize} - 4px)`,height : `calc(350px / ${fieldSize} - 4px)`}}></li>
    </>
}
export {GameFieldBlock}