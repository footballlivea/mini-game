import React from "react";
import { useTranslation } from "react-i18next";
import { Modal } from "react-bootstrap";

const MogalAlerGame = (props) => {
  // Translation
  const { t } = useTranslation();

  return (
    <Modal {...props} className="game-setting-pannel" size="md" aria-labelledby="contained-modal-title-vcenter" centered>
      <Modal.Header closeButton>MineField</Modal.Header>
      <Modal.Body>
        <h2><b>GAME OVER</b></h2>
      </Modal.Body>
    </Modal>
  );
};

export default MogalAlerGame;
