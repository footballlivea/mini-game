import { useContext, useEffect,useState } from "react"
import { ExchangeContext } from "../../contexts/ExchangeContext";
import ModalGameSetting from "./element/ModalGameSetting";
function GamePannel() {

    const {setBalance,balance,win,bet,setBet,setWin,ifActiveGame,setBlock,setFieldArr,setActiveGame} = useContext(ExchangeContext);

    const [showModalGameSetting, setShowModalGameSetting] = useState(false);

    const paly = () => {
        if(bet<=0||bet>balance) return 
        if(ifActiveGame)setBalance(balance+(win-bet))
        setActiveGame()
        setFieldArr()
        setBlock(null)
        setWin(bet)
    };

    const showSetting = () => {
        if(ifActiveGame) return
        setShowModalGameSetting(!showModalGameSetting)
    };

    return <>
        <div className="game-pannel">
            <div className="balances">
                <p className="bal-hed">Balance</p>
                <p>${balance.toFixed(2)}</p>
            </div>

            <div className="btn-block">
            <input className="bet" type="number" step="1" min="1" value={bet} onChange={(e) => setBet(Number(e.target.value))}/>    
                <button onClick={paly}>BET</button>
                <button onClick={showSetting}>setting</button>
            </div>
        </div>
        <ModalGameSetting show={showModalGameSetting} setBlock={setBlock} onHide={() => setShowModalGameSetting(false)} />

    </>
}
export {GamePannel}