import { createContext, useReducer } from "react";
import { Reducer } from "../reducer/Reducer";

export const ExchangeContext = createContext();

const initialState = {
  fieldSize:5,
  countMine:3,
  fieldArr : [],
  ifActiveGame : false,
  block:null,
  bet:1,
  balance:100,
  win:0
};

export const ExchangeProvider = ({ children }) => {
  const [value, dispatch] = useReducer(Reducer, initialState);

  value.setFieldSize = (data) => {
    dispatch({ type: "setFieldSize", payload: data });
  };
  value.setBlock = (data) => {
    dispatch({ type: "setBlock", payload: data });
  };
  value.setCountMine= (data) => {
    dispatch({ type: "setCountMine", payload: data });
  };
  value.setBet= (data) => {
    dispatch({ type: "setBet", payload: data });
  };
  value.setBalance= (data) => {
    dispatch({ type: "setBalance", payload: data });
  };
  value.setWin= (data) => {
    dispatch({ type: "setWin", payload: data });
  };
  value.setActiveGame = () => {
    dispatch({ type: "setActiveGame"});
  };
  value.setFieldArr = () => {
    dispatch({ type: "setFieldArr"});
  };

  return (<ExchangeContext.Provider value={value}>{children}</ExchangeContext.Provider>);
};
