import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";

// Components List

import Home from "../pages/Home";


function Routess() {
  return (
    <>
      <BrowserRouter>
        {/* <NavBar /> */}
        <Routes>
          <Route path="/" element={<Home/>} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default Routess;
