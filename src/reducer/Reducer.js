/* eslint-disable no-lone-blocks */
export function Reducer(state, { type, payload }) {
  switch (type) {
    case "setFieldSize":{
      if(payload === 3) {
        return {
          ...state,
          fieldSize: payload,
          countMine: 1,
        };
      }
      if(payload === 5) {
        return {
          ...state,
          fieldSize: payload,
          countMine: 3,
        };
      }
      if(payload === 7) {
        return {
          ...state,
          fieldSize: payload,
          countMine: 5,
        };
      }
      if(payload === 9) {
        return {
          ...state,
          fieldSize: payload,
          countMine:10 ,
        };
      }
    }
    case "setActiveGame": return {
        ...state,
        ifActiveGame: !state.ifActiveGame,
      };
    case "setBet": return {
        ...state,
        bet: payload,
      };
    case "setWin": return {
        ...state,
        win: payload,
      };
    case "setBalance": return {
        ...state,
        balance: payload,
      };
    case "setBlock": return {
        ...state,
        block: payload,
      };
    case "setCountMine": return {
        ...state,
        countMine: payload,
      };
    case "setFieldArr":{
      let arr = []
      for (let i = 0; i < state.fieldSize*state.fieldSize; i++) {
        arr.push(
          {
            id:0,
            count:i,
          })
      }
      let max = state.fieldSize*state.fieldSize-1
      let min = 0
      var totalNumbers = max - min + 1,
              arrayTotalNumbers   = [],
              arrayRandomNumbers  = [],
              tempRandomNumber;
          while (totalNumbers--) {
              arrayTotalNumbers.push(totalNumbers + min);
          }
          while (arrayTotalNumbers.length) {
              tempRandomNumber = Math.round(Math.random() * (arrayTotalNumbers.length - 1));
              arrayRandomNumbers.push(arrayTotalNumbers[tempRandomNumber]);
              arrayTotalNumbers.splice(tempRandomNumber, 1);
          }
      for (let i = 0; i < state.countMine; i++) {
        let num = arrayRandomNumbers[i]
          arr[num].id=1
      }
      return {
        ...state,
        fieldArr:arr
      }
    }
      

    default:
      return state;
  }
}
